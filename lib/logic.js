/**
 * Where all the logic should be executed (http entry point)
 *
 * @param {Object} reqBody - http request body payload
 * @return {Object} response that will be returned as json
 * @api public
 */

var taxData = require('./tax.json')
var applyLogic = function (reqBody) {
  const body = reqBody.body

  console.log('--------------')
  console.log('logic', body)

  const tax = taxData[body.country]
  const total = calculate(body.prices, body.quantities, tax, 1)

  const totalReduc = total * applyReduc(total, body.reduction);

  const result = {total: round(totalReduc)}
  console.log('result', result)

  return result;
};

function applyReduc(total, reduction) {
  if (reduction === 'STANDARD') {
    if (total >= 50000) {return 0.85}
    if (total >= 10000) {return 0.9}
    if (total >= 7000) {return 0.93}
    if (total >= 5000) {return 0.95}
    if (total >= 1000) {return 0.97}
    return 1
  }
  if (reduction==='PAY THE PRICE') {
    return 1
    //feedback { type: 'ERROR',
    //    content: 'Goddamn, maroilles replied 647.28 but right answer was 647.27. 323.63 will be charged.' }
    //logic { prices: [ 66.93, 53.77, 45.85, 39.73, 22.19, 13.13, 94.18, 13.46, 14.99, 89.4 ],

    }
  if (reduction==='HALF PRICE') {
    return 0.5
  }

  throw new Error('reduction invalid')
}


function round(number) {
  return parseFloat(number.toFixed(2));
}


function calculate(prices, quantities, tax, reduc) {
  if (prices.length !== quantities.length) {
    throw new Error('Price and quantities difer')
  }

  const total = prices.reduce(function (result, price, index) {
    result += price * quantities[index] * (1 + tax)
    return result
  }, 0)

  return total
}


module.exports = applyLogic;
