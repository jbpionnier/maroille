var http = require('http'),
    url = require('url'),
    querystring = require('querystring');


var express = require('express');
var bodyParser = require('body-parser');

function start(conf, applyLogic) {

  var app = express();
  app.use(bodyParser.json());


  app.post('/order', function (req, res) {
    try {
      const result= applyLogic(req)
      if (result) {
        return res.json(result)

      }
      res.status(404).send({error: 'Error'})
    } catch (err) {
      res.status(404).send({error: 'Error'})
    }
  });


  app.post('/feedback', function (req, res) {
    const body = req.body || {}

    if (body.type !== 'INFO') {
      console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
      console.log('feedback ERROR', req.body)
      console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    }
    res.send()
  })

  const server = app.listen(conf.port, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
  });

  return server
}
module.exports.start = start;
